-- |
-- Module: Caesar.Break
-- Copyright: (c) 2018 Julien Dehos
-- License: BSD3
--
-- Caesar cipher, break module.

module Caesar.Break (breakCaesar) where

-- | Find the coding shift used to encrypt a string (TODO).
breakCaesar 
  :: String  -- ^ string to break
  -> Int     -- ^ shift
breakCaesar _ = 13

