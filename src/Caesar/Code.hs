-- |
-- Module: Caesar.Code
-- Copyright: (c) 2018 Julien Dehos
-- License: BSD3
--
-- Caesar cipher, code module.

module Caesar.Code (shiftChar, codeCaesar) where

import Data.Char (chr, isLower, ord)

-- | Encrypt a character.
shiftChar 
  :: Int   -- ^ shift
  -> Char  -- ^ input char
  -> Char  -- ^ encrypted char
shiftChar n c | isLower c = chr $ ord 'a' + ((n + ord c - ord 'a') `mod` 26)
              | otherwise = c

-- | Encrypt a string using caesar cipher.
codeCaesar 
  :: Int     -- ^ shift
  -> String  -- ^ input string
  -> String  -- ^ encrypted string
codeCaesar n = map (shiftChar n)

