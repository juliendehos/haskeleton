-- |
-- Module: Caesar
-- Copyright: (c) 2018 Julien Dehos
-- License: BSD3
--
-- A Haskell implementation of Caesar cipher.
--
-- > import Caesar.Code (codeCaesar)
-- > codeCaesar 1 "hal"

module Caesar (codeCaesar, breakCaesar) where

import qualified Caesar.Code as CC
import qualified Caesar.Break as CB

-- |
-- Same as Caesar.Code.codeCaesar
codeCaesar :: Int -> String -> String
codeCaesar = CC.codeCaesar

-- |
-- Same as Caesar.Break.breakCaesar
breakCaesar :: String -> Int
breakCaesar = CB.breakCaesar

