# haskeleton
[![License MIT](https://img.shields.io/badge/license-MIT-brightgreen.svg)](https://opensource.org/licenses/MIT)
[![Build status](https://gitlab.com/juliendehos/haskeleton/badges/master/build.svg)](https://gitlab.com/juliendehos/haskeleton/pipelines) 


haskell skeleton project (cabal, stack, nix)

## contents

- a library: `haskeleton`

- an executable: `haskeleton-codecontents`

```
$ cabal run haskeleton-codecontents 
...
Running haskeleton-codecontents...
hal
uny
toto
gbgb
```

- some code documentation

![](images/haddock.png)

- some tests (unit tests + quickcheck)

```
$ cabal test 
...
Running 1 test suites...
Test suite spec: RUNNING...
Test suite spec: PASS
Test suite logged to: dist/test/haskeleton-0.1-spec.log
1 of 1 test suites (1 of 1 test cases) passed.
```

- some benchmarks

![](images/criterion.png)


## stack

- setup & build:

```
stack build
```

- run executable (`haskeleton-codecontents`):

```
stack exec haskeleton-codecontents
```

- build doc:

```
stack haddock
firefox ".stack-work/install/x86_64-linux/lts-9.13/8.0.2/doc/haskeleton-0.1/index.html"
```

- run tests:

```
stack test
```

- benchmark:

```
stack bench
```

- benchmark (criterion + html output):

```
./.stack-work/install/x86_64-linux/lts-9.13/8.0.2/bin/haskeleton-criterion --output bench.html
firefox bench.html
```


## nix

- setup & build:

```
nix-shell 
cabal build
```

- run executable (`haskeleton-codecontents`):

```
cabal run haskeleton-codecontents
```

- build doc:

```
cabal haddock
firefox dist/doc/html/haskeleton/index.html
```

- run tests:

```
cabal test
```

- benchmark:

```
cabal bench
```

- benchmark (criterion + html output):

```
cabal build haskeleton-criterion
./dist/build/haskeleton-criterion/haskeleton-criterion --output bench.html
firefox bench.html
```

