import Caesar (codeCaesar)

main :: IO ()
main = getContents >>= putStrLn . (codeCaesar 13)

