import Data.Time
import Caesar

blake :: String
blake = "The eagle never lost so much time as when he submitted to learn of the crow"

main :: IO ()
main = do
  start <- getCurrentTime
  let str = codeCaesar 13 blake
      str' = codeCaesar 13 str
  putStrLn str
  putStrLn str'
  end <- getCurrentTime
  putStrLn $ "code/decode took " ++ show (diffUTCTime end start)

