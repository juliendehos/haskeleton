import Criterion.Main
import Caesar

blake :: String
blake = "The eagle never lost so much time as when he submitted to learn of the crow"

main :: IO ()
main = defaultMain [
  bgroup "codeCaesar" [ 
    bench "1 hal"  $ whnf (codeCaesar 1) "hal",
    bench "13 helloworld"  $ whnf (codeCaesar 13) "Hello World !",
    bench "13 blake" $ whnf (codeCaesar 13) blake
    ]
  ]

