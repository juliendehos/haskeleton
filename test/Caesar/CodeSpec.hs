module Caesar.CodeSpec (main, spec) where

import Test.Hspec
import Test.QuickCheck

import Caesar.Code

main :: IO ()
main = hspec spec

-------------------------------------------------------------------------------
-- test suite
-------------------------------------------------------------------------------

spec :: Spec
spec = do

  describe "codecaesar" $ do

    it "codes hal with shift=1" $ codeCaesar 1 "hal" `shouldBe` "ibm"

    it "codes HAL with shift=1" $ codeCaesar 1 "HAL" `shouldBe` "HAL"

    it "code and decode an *arbitrary* string" $ 
        property prop_code_decode

    it "check the size of an encrypted *arbitrary* string" $ 
        property prop_code_size

-------------------------------------------------------------------------------
-- properties (QuickCheck)
-------------------------------------------------------------------------------

prop_code_decode :: CaesarData -> Bool
prop_code_decode (CaesarData n s) = (codeCaesar (-n) (codeCaesar n s)) == s

prop_code_size :: CaesarData -> Bool
prop_code_size (CaesarData n s) = length (codeCaesar n s) == length s

-------------------------------------------------------------------------------
-- input data generator (QuickCheck)
-------------------------------------------------------------------------------

data CaesarData = CaesarData Int String deriving Show

genSafeInt :: Gen Int
genSafeInt = elements [-26..25]

genSafeString :: Gen String
genSafeString = listOf $ elements (['a'..'z'] ++ ['A'..'Z'] ++ " ,;.?!()")

instance Arbitrary CaesarData where
  arbitrary = CaesarData <$> genSafeInt <*> genSafeString

